<?php

/*
	Plugin Name: DoctorFeature
	Plugin URI: https://www.institut-pandore.com
	Description: Generates a featured picture for each post with a nice gradient.
	Author: André de Hillerin
	Version: 1.2.0
	Author URI: https://cercle.institut-pandore.com/user/theseconddre/
	License: GPL2
	License URI: https://www.gnu.org/licenses/gpl-2.0.html
	Text Domain: drf
	Domain Path: drf

	DoctorFeature is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	any later version.
	 
	DoctorFeature is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	 
	You should have received a copy of the GNU General Public License
	along with DoctorFeature. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
*/

define('GFP_PLUGIN_PATH', plugin_dir_path(__FILE__));
require_once GFP_PLUGIN_PATH . 'vendor/autoload.php';
require_once GFP_PLUGIN_PATH . "DoctorFeaturePicture_settings.php";
require_once GFP_PLUGIN_PATH . "DoctorFeaturePicture_category_settings.php";
require_once GFP_PLUGIN_PATH . "DoctorFeaturePicture.php";

if( is_admin() )
{
	// Settings
	$my_settings_page 		=  new DoctorFeaturePicture_settings();
	$my_category_settings 	=  new DoctorFeaturePicture_category_settings();

	// Generate picture
	$generatedPicture = new DoctorFeaturePicture();
	add_action('publish_post', array($generatedPicture, 'generate_picture'), 10, 2);
}

