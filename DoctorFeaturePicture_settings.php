<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class DoctorFeaturePicture_settings
{
    function __construct()
    {
        add_action( 'after_setup_theme', array($this,'load_after_setup_theme') );
    }

    public function load_after_setup_theme()
    {
        \Carbon_Fields\Carbon_Fields::boot();
        $this->build_settings();
    }

    public function build_settings()
    {
        Container::make( 'theme_options', 'DoctorFeature' )
            ->set_page_parent( 'options-general.php' )
            ->add_tab( __('Text settings'), array
            (
                Field::make( 'html', 'drf_text_introduction' )
                    ->set_html( "<h2>What are <strong>Text Settings</strong>?</h2><p>DoctorFeature allows you to generate thumbnail automatically. It can write the post's title on each thumbnail. Here are some settings to customise thumbnails.</p>" ),

                Field::make( 'checkbox', 'drf_text_displayed', "Write the post's title on the thumbnail?" )
                    ->set_option_value( '1' )
                    ->set_help_text("Ignore settings below if no."),

                Field::make( 'select', 'drf_text_font', 'Text font' )
                    ->add_options( array(
                            '/font/Roboto-Bold.ttf' => 'Roboto Bold',
                            '/font/Roboto-Regular.ttf' => 'Robot Regular',
                            '/font/IndieFlower.ttf' => 'Indie Flower',
                            '/font/Lobster-Regular.ttf' => 'Lobster Regular',) )
                    ->set_required( true )
                    ->set_help_text( __('Font used when generating a thumbnail.', 'drf')),

                Field::make( 'text', 'drf_text_size', 'Text size (px)' )
                    ->set_attribute('type', 'number')
                    ->set_attribute('step', '1')
                    ->set_default_value(72)
                    ->set_required( true )
                    ->set_help_text( __('Font size usef when generating a thumbnail. Recommended: 80.', 'drf')),

                Field::make( 'color', 'drf_text_color', 'Text color' )
                    ->set_required( true )
                    ->set_help_text( __('Color of the title when generating a thumbnail.', 'drf')),
            ))

            ->add_tab( __('Background settings'), array
            (
                Field::make( 'html', 'drf_background_introduction' )
                    ->set_html( "<h2>What are <strong>Background Settings</strong>?</h2><p>DoctorFeature allows you to generate thumbnail automatically. Using those settings, you can change the background of thumbnails.</p>
                        <ul>
                            <li><strong>Gradients :</strong> each category will use a specific gradient color. You can use your own gradient colors by going to a category edit page.</li>
                            <li><strong>Geo Pattern :</strong> each category will use a specific geometric colorized pattern.</li>
                        </ul>" ),
                Field::make( 'radio', 'drf_background_type', 'What kind of background when generating thumbnails?' )
                    ->add_options( array(
                        'pattern' => 'Geometric Patterns',
                        'gradient' => 'Color Gradients',))
            ));
    }
}