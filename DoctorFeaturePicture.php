<?php

use GDText\Box;
use GDText\Color;
use TheSecondDre\PatternCreator;

class DoctorFeaturePicture
{
	// Constant variables
	private $width					=  1200;
	private $height 				=  627;
	private $font_size 				=  70;
	private $font 					=  'font/Roboto-Bold.ttf';
	private $text_color 			=  array(255, 255, 255);
	private $text_displayed			=  true;


	private $background_type		=  'gradient';

	private $is_category_gradient	=  false;
	private $forced_gradient		=  array('#FF0000', '#00FF00', '#0000FF');

	private $url					=  '';		
	private $attachment_id			=  0;
	private $featured_php_img 		=  NULL;

	function __construct() {
	}

	/**
	 * Sets the variables
	 *
	 * @param      integer  $post_ID  The post id
	 */
	private function get_settings($post_ID)
	{
		$options 		= array(
			'size' 		 		=> carbon_get_theme_option('drf_text_size'),
			'font' 	 			=> carbon_get_theme_option('drf_text_font'),
			'color'  			=> carbon_get_theme_option('drf_text_color'),
			'text_displayed' 	=> carbon_get_theme_option('drf_text_displayed'),
			'background_type'	=> carbon_get_theme_option('drf_background_type'),
		);

		// Is there a gradient for category?
		$the_category   = get_the_category($post_ID);
		$cat_id 		= $the_category[0]->term_id;
		$cat_options 	= get_term_meta($cat_id, 'category_gradient', true);
		if ($cat_options != '')
		{
			$pieces = explode(',', $cat_options);
			$this->is_category_gradient			= true;
			$this->forced_gradient 				= $pieces;
		}

		// Global settings
		if ($options['size'] != '') {
			$this->font_size 			= $options['size'];
		}

		if ($options['font'] != '') {
			$this->font 				= $options['font'];
		}

		if ($options['color'] != '') {
			$this->text_color 			= $this->hex2rgb($options['color']);
		}

		if ($options['text_displayed'] != 1) {
			$this->text_displayed 		= false;
		}

		if ($options['background_type'] != '') {
			$this->background_type 	= $options['background_type'];
		}
	}


	/**
	 * Generates a thumbnail for an article
	 *
	 * @param      integer	$post_ID  The post id
	 * @param      WP_Post  $post     The post
	 */
	public function generate_picture($post_ID, $post)
	{
		// Fetch all general settings
		$this->get_settings($post_ID);
		// echo($this->use_patterns); 

		if (has_post_thumbnail($post_ID)) 
		{
			return;					
		}

		else if ($this->background_type == "pattern")
		{
			$pattern = new TheSecondDre\PatternCreator([
				'width' 	=>  $this->width,
				'height' 	=>  $this->height,
				'color' 	=>  '#7700ff',
				'pattern' 	=>  'square3',
			]);
			$this->featured_php_img = $pattern->createPattern('phpgd');

			if ($this->text_displayed == true) {
	        	$this->align_text($post->post_title);
	        }
    		
    		$this->upload_image_wp($post_ID);
    		set_post_thumbnail($post_ID, $this->attachment_id);
		}
		
		else
		{
    		$colors = $this->get_gradient_by_category($post); 
  
	        if (count($colors) == 3) {
	            $this->featured_php_img   =  $this->gradient_3colors_horizontal($this->width, $this->height, $colors);
	        } else if (count($colors) == 2) {
	            $this->featured_php_img = $this->gradient_2colors_vertical($this->width, $this->height, 'v', $colors);
	        }
	        
	        if ($this->text_displayed == true) {
	        	$this->align_text($post->post_title);
	        }
    		
    		$this->upload_image_wp($post_ID);
    		set_post_thumbnail($post_ID, $this->attachment_id);
		}
	}

	/**
	 * Aligns the text as we want it to. Here, a centered alignment occurs.
	 *
	 * @param      string  $title  The title
	 */
	private function align_text($title) {
		$textbox = new Box($this->featured_php_img);
		$textbox->setFontSize($this->font_size);
		$textbox->setFontFace(GFP_PLUGIN_PATH . $this->font);
		$textbox->setFontColor(new Color($this->text_color[0], $this->text_color[1], $this->text_color[2]));
		$textbox->setBox(
		    100,  	// distance from left edge (100px padding)
		    0,  	// distance from top edge
		    imagesx($this->featured_php_img)-200, 	// textbox width, equal to image width (200px as right side padding)
		    imagesy($this->featured_php_img)  		// textbox height, equal to image height
		);

		$textbox->setTextAlign('center', 'center');
		$textbox->draw($title);
	}

	/**
	 * Uploads an image to the WordPress media library
	 */
	private function upload_image_wp($post_ID) {
		$file = 'images/new_featured_image.png'; 
	    imagepng($this->featured_php_img, $file); 
	    $filename = basename($file);
		$upload_file = wp_upload_bits($filename, null, file_get_contents($file));
		$this->url = $upload_file['url'];

		if (!$upload_file['error']) {
			$wp_filetype = wp_check_filetype($filename, null );
			$attachment = array(
				'post_mime_type' 	=> $wp_filetype['type'],
				'post_parent' 		=> $post_ID,
				'post_title' 		=> preg_replace('/\.[^.]+$/', '', $filename),
				'post_content' 		=> '',
				'post_status' 		=> 'inherit'
			);
			$this->attachment_id = wp_insert_attachment( $attachment, $upload_file['file'], $post_ID );
			if (!is_wp_error($this->attachment_id)) {
				require_once(ABSPATH . "wp-admin" . '/includes/image.php');
				$attachment_data = wp_generate_attachment_metadata( $this->attachment_id, $upload_file['file'] );
				wp_update_attachment_metadata( $this->attachment_id,  $attachment_data );
			}
		}
	}

	/**
	 * Converts a hexadecimal color (#XXXXXX) to an array of rgb color ( array($r,$g,$b) )	
	 *		
	 * @param      string  $c      { a hexadecimal color }
	 *
	 * @return     array of string  ( array of rgb color )
	 */
	private function hex2rgb($c)
	{
	    $c = str_replace("#", "", $c);
	    if(strlen($c) == 3) { 
	        $r = hexdec( $c[0] . $c[1] );
	        $g = hexdec( $c[1] . $c[1] );
	        $b = hexdec( $c[2] . $c[1] );
	    } elseif (strlen($c) == 6 ) {
	        $r = hexdec( $c[0] . $c[2] );
	        $g = hexdec( $c[2] . $c[2] );
	        $b = hexdec( $c[4] . $c[2] );
	   	} else {
	        $r = 'ff';
	        $g = 'ff';
	        $b = '00';
	    }

	    return array($r, $g,$b);
	}

	/**
	 * Shows the category of the post 
	 *
	 * @param      WP_Post 	$post_id  The post identifier
	 *
	 * @return     string  	$category The category of the post (example : 'biology')
	 */
	private function get_yoast_primary_cat($post_id = NULL)
    {
        $post_id = ($post_id == NULL) ? get_the_id() : $post_id;

        if ( class_exists('WPSEO_Primary_Term') ) {
            // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
            $wpseo_primary_term = new WPSEO_Primary_Term( 'category', $post_id );
            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
            $term = get_term( $wpseo_primary_term );

            if (is_wp_error($term)) { 
                $categories = get_the_category();
                $category = $categories[0];
            } else { 
                $category = $term;
            }
        } else {
            $categories = get_the_category();
            $category = $categories[0];
        }

        return $category;
    }

    /**
     * Generates a 2-colored gradient that can be vertical or horizontal
     *
     * @param      integer  		$width      The width
     * @param      integer  		$height     The height
     * @param      string  			$direction  The direction
     * @param      array of string  $colors     The colors (array of 2 arrays of rgb colors)
     *
     * @return     resource  		$img		The image resource as a gradient between the 2 colors    */
    private function gradient_2colors_vertical($width, $height, $direction, $colors)
	{
        $img = imagecreatetruecolor($width, $height);
        $color1 = $this->hex2rgb($colors[0]);
        $color2 = $this->hex2rgb($colors[1]);

        if($direction=='h') {
            $size = imagesx($img);
            $sizeinv = imagesy($img);
        } else {
            $size = imagesy($img);
            $sizeinv = imagesx($img);
        }
        $diffs = array(
                (($color2[0]-$color1[0])/$size),
                (($color2[1]-$color1[1])/$size),
                (($color2[2]-$color1[2])/$size)
        );

        for($i=0;$i<$size;$i++) {
            $r = $color1[0]+($diffs[0]*$i);
            $g = $color1[1]+($diffs[1]*$i);
            $b = $color1[2]+($diffs[2]*$i);

            if($direction=='h') {
                imageline($img,$i,0,$i,$sizeinv,imagecolorallocate($img,$r,$g,$b));
            } else {
                imageline($img,0,$i,$sizeinv,$i,imagecolorallocate($img,$r,$g,$b));
            }
        }

        return $img;
	}

	/**
	 * Generates a 3-colored horizontal gradient
	 *
	 * @param      integer  		$width   	The width
	 * @param      integer  		$height  	The height
	 * @param      array of string  $colors  	The colors (array of 3 arrays of hex colors)
	 *
	 * @return     resource			$im  		The image resource as a gradient between the 3 colors
	 */
    private function gradient_3colors_horizontal($width, $height, $colors)
	{
		//if # alors enlever le #
		for($i=0; $i<count($colors); $i++)
		{
			if($colors[$i][0] == '#') $colors[$i] = substr($colors[$i], 1);
		}

	    $colorsCount    =  count($colors) - 1 ;            
	    $limit          =  floor($width / $colorsCount);

	    $im = imagecreatetruecolor($width, $height);

	    list($r, $g, $b)        =  sscanf($colors[0], "%02x%02x%02x");
	    list($r1, $g1, $b1)     =  sscanf($colors[1], "%02x%02x%02x");
	    list($r2, $g2, $b2)     =  sscanf($colors[2], "%02x%02x%02x");

	    for($i = 0; $i < $limit; $i++) {
	        $mycolors[$i] = imagecolorallocate($im, 
	            $r + (($r1 - $r) / $limit) * $i,
	            $g + (($g1 - $g) / $limit) * $i,
	            $b + (($b1 - $b) / $limit) * $i);

	         $mycolors[$i + $limit] = imagecolorallocate($im, 
	            $r1 + (($r2 - $r1) / $limit) * $i,
	            $g1 + (($g2 - $g1) / $limit) * $i,
	            $b1 + (($b2 - $b1) / $limit) * $i);
	    }

	    for ($i=0; $i < $width; $i++) { 
	        imageline($im, $i, 0, $i, $height-1, $mycolors[$i]);
	    }

	    return $im;
	}

	/**
	 * Shows the gradient by category.
	 *
	 * @param      WP_Post  $post   	The post
	 *
	 * @return     array   $colors 	The array of 2 or 3 hex colors.
	 */
	private function get_gradient_by_category($post)
	{
	    $category = $this->get_yoast_primary_cat($post->ID);

	    if($this->is_category_gradient==true)
	    {
	    	return $this->forced_gradient;
	    } else {
	    	$gradients = array(
		    	array('00FF7F', '00BFFF'), 
				array('FFC0CB', 'DC143C', '000080'), 
				array('DC143C', '6A5ACD'), 
				array('FFFF00', '05AE36'), 
				array('FF00FF', '0000CD'),
				array('FF00FF', 'FF4500', 'FFA500'), 
				array('44BFFF', '7B68EE', 'C71585'), 
				array('000080', '0000CD', '00BFFF'), 
				array('000000', '000000', '000000'),
			);

			srand(intval($category->term_id));
			$random = rand(0, 9999) % count($gradients);

			return $gradients[$random]; 
	    }
	}
}