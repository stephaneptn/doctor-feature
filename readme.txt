=== DoctorFeature ===

Contributors: André de Hillerin
Tags: gradient, generator, featured image, category
Requires at least: 3.0
Tested up to: 4.8
Stable tag: 1.0
License: GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html

== Description ==

As soon as you'll save an article that doesn't have any featured image, DoctorFeature will create a nice picture for you. The picture will be composed of a gradient of either two or three beautiful colors, and each gradient will be associated with the category of your article. It will be up to you to select among a few fonts, the size of the text and the text color. The plugin will print the text over the generated picture and you'll have your own featured image ! :-)

== Installation ==

1. Upload `doctorfeature` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Log In to your WordPress Dashboard and go to menu `Dashboard > DoctorFeature > Settings` to setting DoctorFeature

== Frequently Asked Questions ==

= Can I modify the actual gradient or set my own gradient? =
 
Sure! With v1.1.0 you are now able to access the category settings and set your own gradient there. If you don’t, then a random gradient will be associated to your category!

== Changelog ==

= 1.1.0 =
Free to remove the text from the image
Free to set custom gradients for the categories

= 1.0.0 =
Add a featured image to your article