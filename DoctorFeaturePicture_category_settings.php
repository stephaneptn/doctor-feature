<?php
class DoctorFeaturePicture_category_settings
{
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action ( 'edit_category_form_fields', array( $this, 'addTitleFieldToCat') );
        add_action ( 'edited_category', array( $this, 'saveCategoryFields') );
        add_action( 'admin_cat_init', array( $this, 'page_init' ) );
    }

    /**
     * Adds a title field to the category settings.
     *
     * @param      int/WP_Term/object  $term   The term
     */
    public function addTitleFieldToCat($term)
    {
        $category_gradient    =  get_term_meta($term->term_id, 'category_gradient', true);
        $default_colors       =  '';
        ?>
        <div class="wrap"> 
            <tr class="form-field">
                <th>Gradient Selected</th>
                <td>
                    <input type="text" name="category_gradient" value="<?php echo ( $category_gradient != '' && !empty($category_gradient) ) ? esc_attr( $category_gradient) : $default_colors; ?>" />
                    <p class = description> <?php print __('Colors of the gradient you wish to set as hexadecimal. You may choose 2 or 3 colors. For instance : #FF0000,#00FF00,#0000FF (no spaces!)', 'drf'); ?> </p>
                </td>
            </tr>
        </div>
        <?php
    }
    

    /**
     * Saves the category fields when the page is updated.
     */
    public function saveCategoryFields() 
    {
        if ( isset( $_POST['category_gradient'] ) ) 
        {
            $category_gradient = trim($_POST['category_gradient'], ", ");
            $category_gradient = preg_replace("/, /", ",", $category_gradient);
            $_POST['category_gradient'] = $category_gradient;

            $array_of_cat_gradient = explode(",", $category_gradient);

            if(count($array_of_cat_gradient)!=1)
            {
                array_splice($array_of_cat_gradient, 3);
                $category_gradient = implode($array_of_cat_gradient, ",");
                $_POST['category_gradient'] = $category_gradient;
            } else {
                $new_array_of_cat_gradient = array( 
                                        $array_of_cat_gradient[0], 
                                        $array_of_cat_gradient[0] );
                $category_gradient = implode($new_array_of_cat_gradient, ",");
                $_POST['category_gradient'] = $category_gradient;
            }

            update_term_meta($_POST['tag_ID'], 'category_gradient', $_POST['category_gradient']);
        }
    }
}
?>